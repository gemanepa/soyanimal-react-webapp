import React from 'react'
import { Link } from 'gatsby'
import github from '../img/github-icon.svg'
import facebook from '../img/navbar/facebook.svg'
import instagram from '../img/navbar/instagram.svg'
import twitter from '../img/navbar/twitter.svg'
import logo from '../img/logo.png'

const Navbar = class extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      active: false,
      navBarActiveClass: '',
    }
  }

  toggleHamburger = () => {
    // toggle the active boolean in the state
    this.setState(
      {
        active: !this.state.active,
      },
      // after state has been updated,
      () => {
        // set the class in state for the navbar accordingly
        this.state.active
          ? this.setState({
              navBarActiveClass: 'is-active',
            })
          : this.setState({
              navBarActiveClass: '',
            })
      }
    )
  }

  render() {
    return (
      <nav
        className="navbar is-transparent"
        role="navigation"
        aria-label="main-navigation"
      >
        <div className="container">
          <div className="navbar-brand">
            <Link to="/" className="navbar-item" title="Logo">
              <img src={logo} alt="Soy Animal Chaco" style={{ width: 'auto', height: '150%' }} />
            </Link>
            {/* Hamburger menu */}
            <div
              className={`navbar-burger burger ${this.state.navBarActiveClass}`}
              data-target="navMenu"
              onClick={() => this.toggleHamburger()}
            >
              <span />
              <span />
              <span />
            </div>
          </div>
          <div
            id="navMenu"
            className={`navbar-menu ${this.state.navBarActiveClass}`}
          >
            <div className="navbar-start has-text-centered">
              <Link className="navbar-item" to="/">
                Inicio
              </Link>
              <Link className="navbar-item" to="/conocenos">
                Conocenos
              </Link>
              <Link className="navbar-item" to="/campañas">
                Campañas
              </Link>
              <Link className="navbar-item" to="/noticias">
                Noticias
              </Link>
              <Link className="navbar-item" to="/comunicate">
                Comunicate
              </Link>
              {/* <Link className="navbar-item" to="/contact/examples">
                Form Examples
              </Link> */}
            </div>
            <div className="navbar-end has-text-centered">
              <a
                className="navbar-item"
                href="https://facebook.com/soyanimal.chaco"
                target="_blank"
                rel="noopener noreferrer"
                title="Facebook"
              >
                <span className="icon">
                  <img src={facebook} alt="Facebook" />
                </span>
              </a>
              <a
                className="navbar-item"
                href="https://instagram.com/soyanimal.chaco"
                target="_blank"
                rel="noopener noreferrer"
                title="Instagram"
              >
                <span className="icon">
                  <img src={instagram} alt="Instagram" />
                </span>
              </a>
              <a
                className="navbar-item"
                href="https://twitter.com/SoyAnimalChaco"
                target="_blank"
                rel="noopener noreferrer"
                title="Twitter"
              >
                <span className="icon">
                  <img src={twitter} alt="Twitter" />
                </span>
              </a>
            </div>
          </div>
        </div>
      </nav>
    )
  }
}

export default Navbar
