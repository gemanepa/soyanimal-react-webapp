import React from 'react'
import Helmet from 'react-helmet'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'
import './all.sass'
import useSiteMetadata from './SiteMetadata'

const TemplateWrapper = ({ children }) => {
  const { title, description } = useSiteMetadata()
  return (
    <div>
      <Helmet>
        <html lang="en" />
        <title>{`${title} - Los animales no tienen voz, pero tienen muchos amigos`}</title>
        <meta name="description" content={description} />
        <meta name="keywords" content="soyanimal,animales,chaco,corrientes,argentina,ong,organizacion,protectora,perros,gatos,monos, caballos" />
        <meta name="author" content="gemanepa" />
        <meta name="theme-color" content="#000000" />

        <link
          rel="icon"
          type="image/png"
          href="/img/favicon-32x32"
          sizes="32x32"
        />
        <link
          rel="icon"
          type="image/png"
          href="/img/favicon-16x16"
          sizes="16x16"
        />

        <meta name="theme-color" content="#fff" />

        <meta property="og:type" content="business.business" />
        <meta property="og:title" content={title} />
        <meta property="og:url" content="/" />
        <meta property="og:image" content="/img/og-image.jpg" />
      </Helmet>
      <Navbar />
      <div>{children}</div>
      <Footer />
    </div>
  )
}

export default TemplateWrapper
