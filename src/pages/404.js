import React from 'react'
import Layout from '../components/Layout'

const NotFoundPage = () => (
  <Layout>
    <div>
      <h1>NOT FOUND</h1>
      <p>Esta pagina no existe.</p>
    </div>
  </Layout>
)

export default NotFoundPage
